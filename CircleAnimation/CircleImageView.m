//
//  CustomImageView.m
//  CircleAnimation
//
//  Created by MacBook Pro on 03.07.13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import "CircleImageView.h"

@implementation CircleImageView {
    
    // _deltaAngle - is the amount of radians by which rotation is performed on every timer call
    float _deltaAngle;
    
    // _currentAngle -- current rotation angle in radians
    float _currentAngle;
    
    // _midAngle -- angle created with points (_anchorPos.x, 0), (_anchorPos.x, _anchorPos.y), (self.frame.width, 0)
    float _midAngle;
    
    // _midAngle2 -- angle created with points (_anchorPos.x, self.frame.height), (_anchorPos.x, _anchorPos.y), (self.frame.width, self.frame.height)
    float _midAngle2;
    
    // _midAngle3 -- angle created with points (_anchorPos.x, self.frame.height), (_anchorPos.x, _anchorPos.y), (self.frame.width, self.frame.height)
    float _midAngle3;
    
    // _midAngle4 -- angle created with points (_anchorPos.x, _anchorPos.y), (0, 0), (0, _anchorPos.y)
    float _midAngle4;
    
    // _timer is used to draw the image on view. FPS = 60
    NSTimer *_timer;
    
    // FPS
    float _timerSpeed;
    
    // _prevTimeInterval is used to keep constant drawing in drawrect
    // default timer doesn't garantee proper time interval calls, so _prevTimerInterval will help us to calculate _deltaAngle
    float _prevTimeInterval;
    
    CGPoint _anchorPos;
}

- (id)init {

    if (self = [super init]) {
        // by default animation speed is 1
        self.animationSpeed = 1.f;
    }
    
    return self;
}

- (void)animate {
    
    [_timer invalidate];
    
    // FPS = 60 - timer will be called 60 times per second
    _timerSpeed = 0.01f;
    
    // anchorPos - is CGPoint on the view based on the value of self.anchorPoint.
    // from (0, 0) to (self.frame.size.width, self.frame.size.height)
    _anchorPos  = CGPointMake(self.frame.size.width * self.anchorPoint.x, self.frame.size.height * self.anchorPoint.y);
    
    // animation starts at angle 0.0 rad and end at the angle 2*M_PI
    _currentAngle = 0.0;
    
    // on every timer call increase currentAngle by 0.65 * snimationSpeed degrees
    float degreesPerTimerCall = .65f;
    _deltaAngle   = M_PI * self.animationSpeed * degreesPerTimerCall / 180.0;
    
    _midAngle  = atanf((self.frame.size.width - _anchorPos.x) / _anchorPos.y);
    _midAngle2 = M_PI/2 + atanf((self.frame.size.height - _anchorPos.y) / (self.frame.size.width - _anchorPos.x));
    _midAngle3 = M_PI + atanf(_anchorPos.x / (self.frame.size.height - _anchorPos.y));
    _midAngle4 = 3./2.*M_PI + atanf(_anchorPos.x / _anchorPos.y);
    
    _prevTimeInterval = [[NSDate date] timeIntervalSince1970];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timerSpeed target:self selector:@selector(rotator) userInfo:nil repeats:YES];
}

- (void)rotator {
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    float currentTimeInterval = [[NSDate date] timeIntervalSince1970];
    float delta               = currentTimeInterval - _prevTimeInterval;
    _prevTimeInterval         = currentTimeInterval;
    
    if (delta > 0) {
        _currentAngle += _deltaAngle * (delta / _timerSpeed);
    } else {
        _currentAngle += _deltaAngle;
    }
    
    float width  = rect.origin.x;
    float height = rect.origin.y;
    BOOL done = NO;
    if (_currentAngle < _midAngle) {
        width = tanf(_currentAngle)*_anchorPos.y + _anchorPos.x;
        height = rect.origin.y;
    } else if ((_currentAngle >= _midAngle) && (_currentAngle < M_PI/2)) {
        height = _anchorPos.y - tanf(M_PI/2 - _currentAngle) * (rect.size.width - _anchorPos.x);
        width = rect.size.width;
    } else if ((_currentAngle >= M_PI/2) && (_currentAngle < _midAngle2)) {
        height = tanf(_currentAngle - M_PI/2)*(rect.size.width - _anchorPos.x) + _anchorPos.y;
        width = rect.size.width;
    } else if ((_currentAngle >= _midAngle2) && (_currentAngle < M_PI)) {
        width = tanf(M_PI - _currentAngle)*(rect.size.height - _anchorPos.y) + _anchorPos.x;
        height = rect.size.height;
    } else if ((_currentAngle >= M_PI) && ((_currentAngle < _midAngle3))){
        width = _anchorPos.x - tanf(_currentAngle - M_PI)*(rect.size.height - _anchorPos.y);
        height = rect.size.height;
    } else if ((_currentAngle >= _midAngle3) && (_currentAngle < 3./2.*M_PI)){
        width = rect.origin.x;
        height = _anchorPos.y + tanf(3./2.*M_PI - _currentAngle) * _anchorPos.x;
    } else if ((_currentAngle >= 3./2.*M_PI) && (_currentAngle < _midAngle4)){
        width = rect.origin.x;
        height = _anchorPos.y - tanf(_currentAngle - 3./2.*M_PI) * _anchorPos.x;
    } else if ((_currentAngle >= _midAngle4) && (_currentAngle < 2*M_PI)){
        width = _anchorPos.x - tanf(2*M_PI - _currentAngle) * _anchorPos.y;
        height = rect.origin.y;
    } else {
        done = YES;
        [_timer invalidate];
        [self.delegate didFinishAnimatingView:self];
    }
    
    [self.delegate didRotateToAngle:_currentAngle];
    
    //---------------------
    
    CGMutablePathRef a_path = CGPathCreateMutable();
    CGContextBeginPath(context);
    
    CGContextMoveToPoint(context, _anchorPos.x, _anchorPos.y);
    CGContextAddLineToPoint(context, _anchorPos.x, rect.origin.y);
    
    if ((_currentAngle > _midAngle) || done) {
        CGContextAddLineToPoint(context, rect.size.width, rect.origin.y);
    }
    if ((_currentAngle > _midAngle2) || done) {
        CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
    }
    if ((_currentAngle > _midAngle3) || done) {
        CGContextAddLineToPoint(context, rect.origin.x, rect.size.height);
    }
    if ((_currentAngle > _midAngle4) || done) {
        CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y);
    }
    CGContextAddLineToPoint(context, width, height);
    
    if (done) {
        CGContextAddLineToPoint(context, _anchorPos.x, rect.origin.y);
    }
    CGContextClosePath(context);
    CGContextAddPath(context, a_path);
    
    // Fill the path
    UIImage *img = [UIImage imageNamed:self.imageName];
    CGContextSetFillColorWithColor(context, [[UIColor colorWithPatternImage:img] CGColor]);
    CGContextFillPath(context);
    CGPathRelease(a_path);
}

@end
