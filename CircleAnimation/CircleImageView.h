//
//  CustomImageView.h
//  CircleAnimation
//
//  Created by MacBook Pro on 03.07.13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AnimationProtocol

- (void)didFinishAnimatingView:(UIView *)view;

@optional

// returns current angle in radians
- (void)didRotateToAngle:(CGFloat)angle;

@end

@interface CircleImageView : UIView

// animationSpeed is angular speed -- is measured in radians per seconds
// (don't confuse with linear)
@property (readwrite) CGFloat animationSpeed;

@property (nonatomic, strong) NSString *imageName;

/* animation anchor point
 * (0, 0) - upper left corner
 * (1, 1) - lower right corner
*/
@property CGPoint anchorPoint;

@property (nonatomic, weak) id<AnimationProtocol> delegate;

// starts animation
- (void)animate;

@end
