//
//  ViewController.h
//  CircleAnimation
//
//  Created by MacBook Pro on 03.07.13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;

- (IBAction)animate:(id)sender;
- (IBAction)stepperPressed:(id)sender;

@end
