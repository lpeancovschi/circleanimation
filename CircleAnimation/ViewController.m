//
//  ViewController.m
//  CircleAnimation
//
//  Created by MacBook Pro on 03.07.13.
//  Copyright (c) 2013 Home. All rights reserved.
//

#import "ViewController.h"
#import "CircleImageView.h"


@implementation ViewController {

    CircleImageView *_customImageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(30.f, 110.f, 284., 328);
    CGPoint anchorPoint = CGPointMake(.5, .5);
    
    _customImageView = [[CircleImageView alloc] init];
    _customImageView.imageName = @"beermug.jpg";
    _customImageView.backgroundColor = [UIColor clearColor];
    _customImageView.frame = frame;
    _customImageView.anchorPoint = anchorPoint;
    [self.view addSubview:_customImageView];
    [self.view sendSubviewToBack:_customImageView];
    
    [_customImageView animate];
}

- (IBAction)animate:(id)sender {
    _customImageView.alpha = 1.f;
    [_customImageView animate];
}

- (IBAction)stepperPressed:(id)sender {
    
    UIStepper *stepper = (UIStepper *)sender;
    self.speedLabel.text = [NSString stringWithFormat:@"Speed: %.1f", stepper.value];
    
    _customImageView.animationSpeed = stepper.value;
}


@end
