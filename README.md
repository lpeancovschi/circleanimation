CircleAnimation
===============

UIImageView loading animation
---------------

CircleImageView is UIView subclass. It allows you to animate it's appearance by rendering in clockwise direction.

[](https://www.dropbox.com/s/kq94snwywonpl24/animation.gif)

Usage example:
--------------

// init image view

```Objective-C
CircleImageView *_customImageView = [[CircleImageView alloc] init];
_customImageView.imageName = @"beermug.jpg";
_customImageView.backgroundColor = [UIColor clearColor];
_customImageView.frame = frame;
_customImageView.anchorPoint = anchorPoint;
[self.view addSubview:_customImageView];

// animate

[_customImageView animate];



Licence: MIT
